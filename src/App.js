import { useState, useEffect } from "react" 
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import './App.css';
import About from "./component/About";
import AddTache from "./component/AddTache";
import Footer from "./component/Footer";
import Header from './component/Header';
import Tasks from './component/Tasks';

function App() {
  const[showAddTask,setShowAddTask] = useState(false)
  const [tasks, setTasks] = useState([])

  useEffect(() => {
    const getTasks = async () =>{
      const tasksFromServer = await fetchTasks()
      setTasks(tasksFromServer)
    }
  
    getTasks()
  }, [])

 // fetch data
 const fetchTasks = async () => {
  const res  = await fetch('http://localhost:5000/tasks')
  const data = await res.json()
  return data

}
 // fetch data
 const fetchTask = async (id) => {
  const res  = await fetch(`http://localhost:5000/tasks/${id}`)
  const data = await res.json()
  return data

}



// supprimer tâche
 const deleteTache = async (id) => {
   await fetch(`http://localhost:5000/tasks/${id}`, {
     method: 'DELETE',
   })

   setTasks(tasks.filter((task) => task.id !==id))
 }

 // toggle Reminder
 const toggleRimender = async (id) =>{
    const taskToToggle = await fetchTask(id) 
    const updTask = {...taskToToggle,
      reminder: !taskToToggle.reminder }
    
    const res = await fetch(`http://localhost:5000/tasks/${id}`, {
      method: 'PUT',
      headers:{
        'content-type': 'application/json',
      },
      body: JSON.stringify(updTask)
    })

    const data = await res.json()


    setTasks(tasks.map((task) => task.id === id
    ? { ...task, reminder:data.reminder} : task))
 }

 // ajouter Tâche
 const addTache = async (task) =>{
   const res = await fetch('http://localhost:5000/tasks',{
     method: 'POST',
     headers: {
       'content-type': 'application/json'
     },
     body: JSON.stringify(task)
   })

   const data = await res.json()
   setTasks([...tasks, data])

  //  const id = Math.floor(Math.random() * 10000) +1 
  //  const newTask = { id, ...task}
  //  setTasks([...tasks, newTask])
 }
 
  return (
  
   <Router>
        <div className="container">
          <Header onAdd={() => setShowAddTask(!showAddTask)} showAdd = {showAddTask}/>
          <Routes>
          <Route path='/'  element={
           
             <>
              { showAddTask && <AddTache onAdd={addTache}/>}
              {tasks.length > 0 ? <Tasks tasks={tasks} onDelete={deleteTache}
              onToggle={ toggleRimender}/> : 'Pas de Tâche'}
            </>
            

          }/>
          <Route path='/about' element={<About/>}/>
          </Routes>
          <Footer/>
        </div>
   </Router>
 
  );
}

export default App;
