 import PropTypes from 'prop-types'
const Button = ({ color , texte, onClick}) => {
  return (
    <div>
      <button onClick={ onClick} style={{ backgroundColor: color }} className='btn'>{texte}</button>
    </div>
  )
}

export default Button

Button.defaultProps = {
    color: 'steelblue',
}

Button.propTypes={
    texte:PropTypes.string,
    color:PropTypes.string,
    onClick:PropTypes.func, 
}