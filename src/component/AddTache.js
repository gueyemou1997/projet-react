import { useState } from "react"

const AddTache = ({ onAdd }) => {
    const [text , setText] = useState('')
    const [day , setDay] = useState('')
    const [reminder , setReminder] = useState(false)
    const onSubmit = (e) =>{
        e.preventDefault()
        if (!text) {
            alert("SVP! Ajouter une tâche")
            return
        }

        onAdd({text, day , reminder})
        setText('')
        setDay('')
        setReminder(false)
    }
  return (
    <form className='add-form' onSubmit={onSubmit}>
         <div className='form-control'>
             <label for="a">Ajouter Tâche</label>
             <input type="text"  placeholder='Ajauter Tâche' id='a' value={text} onChange={(e) => setText(e.target.value)}/>
         </div>
         <div className='form-control'>
         <label for="b">Ajouter Date et heure</label>
             <input type="text" placeholder='Ajauter Date et Heure' id='b' value={day} onChange={(e) => setDay(e.target.value)}/>
         </div>
         <div className='form-control form-control-check'>
         <label for="c">Rappel</label>
             <input type="checkbox" checked={reminder} id='c' value={reminder} onChange={(e) => setReminder(e.currentTarget.checked)}/>
         </div>
         <input type="submit" value="Save Task" className='btn btn-block'/>
          
    </form>
  )
} 

export default AddTache